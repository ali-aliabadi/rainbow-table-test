from django.db import models


class MyUser(models.Model):
    username = models.CharField(max_length=30)
    password = models.TextField()
    joining_time = models.DateTimeField('date of joining')
